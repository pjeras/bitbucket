	function plotas(){
		document.getElementById("rezultatas").style.fontSize = "16px";
		document.getElementById("rezultatas").style.color = "white";
		document.getElementById("rezultatas").style.backgroundColor = "grey";
		
		var plotis = Number(document.getElementsByName("plotis")[0].value);
		var aukstis = Number(document.getElementsByName("aukstis")[0].value);
		if (plotis!=""&&aukstis!=""){
			var perimetras = (plotis+aukstis)*2;
			var istrizaine = Math.round(Math.sqrt(plotis*plotis+aukstis*aukstis)*100)/100;
			document.getElementById("rezultatas").innerHTML = "Stačiakampio plotas: "+plotis*aukstis+"<br>Stačiakampio perimetras: "+perimetras+"<br>Stačiakampio įstrižainė: "+istrizaine;
		}else{
			document.getElementById("rezultatas").innerHTML = "Klaida: Įveskite abu skaičius"
		}
	}
	
	function autoGreitis(){
		var autoInfo = ["2017-07-06 19:59:45", "LRS123"];
		autoInfo.push(Number(document.getElementsByName("kelias")[0].value));
		autoInfo.push(Number(document.getElementsByName("laikas")[0].value));
		var autoGreitis = Math.round((autoInfo[2]/1000)/(autoInfo[3]/60/60));
		
		document.getElementById("autoInfo").innerHTML = "Laikas: "+autoInfo[0] + "<br>Numeriai: "+autoInfo[1]+"<br/>";
		document.getElementById("greitis").innerHTML = "Nuvažiuotas kelias: " + autoInfo[2]+"<br/>Sugaištas laikas sekundėmis: "+autoInfo[3]+"<br/>Greitis: " + autoGreitis + " km/h";
	}
	
	function trikampioFunkcija(){
		var krastine1 = Number(document.getElementsByName("krastine1")[0].value);
		var krastine2 = Number(document.getElementsByName("krastine2")[0].value);
		var krastine3 = Number(document.getElementsByName("krastine3")[0].value);
		
		var rezultatas;
		
		if(krastine1==0||krastine2==0||krastine3==0){
			rezultatas = "čia ne trikampis";
		} else if(krastine1==krastine2&&krastine2==krastine3){
			rezultatas = "lygiakraštis";
		} else if (krastine1==krastine2||krastine1==krastine3||krastine2==krastine3){
			rezultatas = "lygiašonis";
		} else if (krastine1!=krastine2!=krastine3){
			rezultatas = "įvairiakraštis";
		} else {
			rezultatas = "kažkas negerai";
		}
	
		var p = (krastine1+krastine2+krastine3)/2;	//pusperimetris
		var trikampioPlotas = Math.round(Math.sqrt(p*(p-krastine1)*(p-krastine2)*(p-krastine3))*100)/100;
		
		var isStatus;
		var trikampioPlotas2 = Math.round(krastine1*krastine2/2*100)/100;
		var trikampioPlotas3 = Math.round(krastine3*krastine2/2*100)/100;
		if (trikampioPlotas == trikampioPlotas2||trikampioPlotas == trikampioPlotas3){
			if(krastine1==0||krastine2==0||krastine3==0){
				isStatus = " ";
			} else {
				isStatus = ", trikampis yra status";
			}
		}else{
			isStatus = ", trikampis nėra status";
		}
		
		document.getElementById("trikampis").innerHTML = "Trikampis yra: " + rezultatas + ", jo plotas " + trikampioPlotas + isStatus;
	}
	
	var isOn = false;
	
	function light(sw){
		if (isOn){
			document.getElementsByTagName("i")[0].setAttribute("class", "fas fa-toggle-on fa-3x");
			isOn = false;
		} else {
			document.getElementsByTagName("i")[0].setAttribute("class", "fas fa-toggle-off fa-3x");
			isOn = true;
		}
	}