var klausimai = [
	{klausimas: "Kuris iš šių būdų yra teisingas html faile nurodant kelią į JavaScript failą?",
	atsakymai: [
					"a) <script name=\"script.js\"></script>",
					"b) <script href=\"script.js\"></script>",
					"c) <script src=\"script.js\"></script>"],
	teisingasAtsakymas: 2,
	vartotojoAtsakymas: null,
	imgUrl: ""
	},
	{klausimas: "Kuriuo iš šių metodų galime pasirinkti div elementus?",
	atsakymai: [
					"a) document.getElementById(\"id\")",
					"b) document.getElementsByClassName(\"name\") ",
					"c) document.getElementsByTagName(\"name\")"],
	teisingasAtsakymas: 2,
	vartotojoAtsakymas: null,
	imgUrl: ""
	},
	{klausimas: "Kas yra argumentas?",
	atsakymai: [
				"a) Konkreti reikšmė paduodama funkcijai veiksmams atliktams, pvz: 10",
				"b) Funkcijos deklaravime vardas suteiktas reikšmei, kuris yra paduodamas funkcijai, kai iškviečiama funkcija, pvz: ilgis",
				"c) Kintamasis, kuris grąžinamas su raktažodžiu return"],
	teisingasAtsakymas: 0,
	vartotojoAtsakymas: null,
	imgUrl: ""
	},
	{klausimas: "Kokia bus x reikšmė?",
	atsakymai: [
					"a) 4",
					"b) 24",
					"c) 1"],
	teisingasAtsakymas: 1,
	vartotojoAtsakymas: null,
	imgUrl: "https://image.ibb.co/buDMbd/klausimas.png"
	},
	{klausimas: "Kas turi būti įrašyta pabrauktoj vietoje, kad atspausdintume skaičius nuo 1 iki 5: ",
	atsakymai: [
					"a) for",
					"b) while",
					"c) if"],
	teisingasAtsakymas: 1,
	vartotojoAtsakymas: null,
	imgUrl: "https://image.ibb.co/bHbw9y/klausimas2.png"
	},
	{klausimas: "Koks bus rezultatas?",
	atsakymai: [
					"a) 10",
					"b) 9",
					"c) 0"],
	teisingasAtsakymas: 1,
	vartotojoAtsakymas: null,
	imgUrl: "https://image.ibb.co/nyZSGd/klausimas3.png"
	},
	{klausimas: "Koks bus rezultatas? console.log(Math.sqrt(9));",
	atsakymai: [
					"a) 81",
					"b) 9",
					"c) 3"],
	teisingasAtsakymas: 2,
	vartotojoAtsakymas: null,
	imgUrl: ""
	},
	{klausimas: "Kiek kartų bus įvykdytas pakartojimas?",
	atsakymai: [
					"a) 0",
					"b) 1",
					"c) begalinį"],
	teisingasAtsakymas: 2,
	vartotojoAtsakymas: null,
	imgUrl: "https://image.ibb.co/drVOpy/klausimas4.png"
	},
	{klausimas: "Kuris iš šių metodų teisingas pridedant naują elementą į vaisiai = [\"Obuoliai\", \"Apelsinai\", \"Bananai\", \"Kiviai\"]",
	atsakymai: [
					"a) vaisiai.push[\"Citrinos\"]",
					"b) vaisiai.value = \"Citrinos\"",
					"c) vaisiai[4] = \"Citrinos\""],
	teisingasAtsakymas: 0,
	vartotojoAtsakymas: null,
	imgUrl: ""
	},
	{klausimas: "Kam bus lygus kintamasis boolean?",
	atsakymai: [
					"a) true",
					"b) false",
					"c) undefined"],
	teisingasAtsakymas: 0,
	vartotojoAtsakymas: null,
	imgUrl: "https://image.ibb.co/jnDyNJ/klausimas5.png"
	}
];

var konteineris = document.getElementsByClassName("konteineris")[0];
var enabled = true;

for(var x=0; x<klausimai.length; x++){

	var klausimas = document.createTextNode(klausimai[x].klausimas);
	var kitasKlausimas = document.createElement("DIV");
	kitasKlausimas.setAttribute("klausimoNr", x);
	kitasKlausimas.classList.add("klausimas", "border", "border-primary", "rounded");
	
	var klausimo = document.createElement("DIV");
	klausimo.classList.add("h5");
	klausimo.appendChild(klausimas);
	kitasKlausimas.appendChild(klausimo);
	konteineris.appendChild(kitasKlausimas);
	
	//line darom
	var line = document.createElement("hr");
	kitasKlausimas.appendChild(line);
	
	
	//itraukiam img
	var imgElem = document.createElement("IMG");
	imgElem.src = klausimai[x].imgUrl;
	imgElem.style.cssFloat = "right";
	kitasKlausimas.appendChild(imgElem);
	
	//atsakymu sudejimas i div
	var atsakymuDiv = document.createElement("DIV");
	if(klausimai[x].imgUrl !== ""){
		atsakymuDiv.style.width = "65%";
	}
	kitasKlausimas.appendChild(atsakymuDiv);
	
	for(var i=0; i<klausimai[x].atsakymai.length; i++){
		var paragrafas = document.createElement("P");
		var atsakymas = document.createTextNode(klausimai[x].atsakymai[i]);
		paragrafas.setAttribute("atsakymas", i);
		paragrafas.appendChild(atsakymas);
		atsakymuDiv.appendChild(paragrafas);
	}
		atsakymoPasirinkimas(x);
}

function atsakymoPasirinkimas(param){
	var atsakymoParinkimas = document.getElementsByClassName("klausimas")[param].querySelectorAll("p");
	
	for (var y=0; y<atsakymoParinkimas.length; y++){
		atsakymoParinkimas[y].addEventListener("click", function(){
					atsakymoParinkimas.forEach(function(element){
						element.classList.remove("pasirinktas");
					});
					if(enabled === true){
						this.classList.add("pasirinktas");
						klausimai[param].vartotojoAtsakymas = this.getAttribute("atsakymas");
					}
				});
	}	
}

var rezultatas = document.getElementById("rezultatas");

rezultatas.addEventListener("click", rodytiRezultata);

var rezultatai = document.getElementsByClassName("rezultatai")[0];
var wrapper = document.getElementsByClassName("wrapper")[0];
var ivertinimoID = document.getElementById("ivertinimoID");

var tikrinti = document.getElementById("tikrinti");
tikrinti.addEventListener("click", tikrintiFunk);

function tikrintiFunk(){
	
	enabled = false;
	
	rezultatai.style.display = "none";
	wrapper.style.display = "block";
	for(var e = 0; e<klausimai.length; e++){
		if(klausimai[e].vartotojoAtsakymas != klausimai[e].teisingasAtsakymas){
			
			var ieskau = document.getElementsByClassName("klausimas")[e].querySelectorAll("p");
			for(var r=0; r<ieskau.length; r++){
				if(ieskau[r].getAttribute("class") == "pasirinktas"){
					ieskau[r].className = "neteisingas";
				} else if (ieskau[r].getAttribute("atsakymas")== klausimai[e].teisingasAtsakymas){
					ieskau[r].className = "teisingas";
				} else{
					
				}
			}
		}
	}
	
}

function skaiciuotiRezultata(){
	var teisingiAtsakymai = 0;
	
	for(var q = 0; q<klausimai.length; q++){
		if(klausimai[q].vartotojoAtsakymas == klausimai[q].teisingasAtsakymas){
			teisingiAtsakymai++;
		}
	}
	return Math.round(teisingiAtsakymai/klausimai.length*100);
}

function rodytiRezultata(){
	var ivertinimas = skaiciuotiRezultata();
	rezultatai.style.display = "block";
	wrapper.style.display = "none";
	ivertinimoID.innerHTML = ivertinimas+" %";
}

var perNauja = document.getElementById("naujasTestas");

perNauja.addEventListener("click", perNaujaFunk);

function perNaujaFunk(){
	
	for (var o=0; o<klausimai.length; o++){
		klausimai[o].vartotojoAtsakymas = null;
	}
	
	enabled = true;
	
	var valyti = document.querySelectorAll("p");
	
	for(var w=0; w<valyti.length; w++){
		valyti[w].classList.remove("pasirinktas");
		valyti[w].classList.remove("teisingas");
		valyti[w].classList.remove("neteisingas");
	}
	
}
